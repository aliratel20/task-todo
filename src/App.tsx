import React, { useEffect, useState } from 'react'
import { TodoList } from "./components/TodoList"
import toast, { Toaster } from 'react-hot-toast'
import { NavBar } from './components/NavBar'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import { SignUp } from './components/Auth/SignUp/SignUp'
import { auth, db } from './components/firebase'
import { useDispatch, useSelector } from 'react-redux'
import { loginUser, setLoading, setTasks, setUsername } from './features/userSlice'
import { Login } from './components/Auth/Login/Login'
import { RootState } from './app/store'
import { Todo } from './components/Todo'
import { collection, onSnapshot } from 'firebase/firestore'

export const App: React.FC = () => {

  const dispatch = useDispatch();
  const load = useSelector((state: RootState) => state.data.user.isLoading)
  const taskId = useSelector((state:RootState)=>state.data.user.taskID)
  const [dots,setDots] = useState<string>('.');

  useEffect(() => {
    let i = 0;
    const intervalId = setInterval(() => {
      if (i === 0) {
        setDots('.');
        i++;
      } else if (i === 1) {
        setDots('..');
        i++;
      } else {
        setDots('...');
        i = 0;
      }
    }, 200);

    return () => clearInterval(intervalId);  
  }, []);

  useEffect(() => {
    dispatch(setLoading(false));
    auth.onAuthStateChanged((authUser) => {
      if (authUser) {
        dispatch(
          loginUser({
            uid: authUser.uid,
            username: authUser.displayName,
            email: authUser.email,
          }),
        );
        dispatch(setLoading(false));
        dispatch(setUsername(authUser?.displayName))
        // dispatch(setLoading(false));
        const userId = authUser.uid;
        const todoCollection = collection(db, 'users', userId, 'todos');
        const unsubscribeFirestore = onSnapshot(todoCollection, (querySnapshot) => {
          const tododetails = querySnapshot.docs.map((doc) => {
            const data = doc.data();
            return {
              todoid: data.todoid,
              task: data.task,
              desc: data.desc,
              bdate: data.bdate,
              edate: data.edate,
              done: data.done,
              type: data.type
            };
          });
          dispatch(setTasks(tododetails));
        });
        return () => unsubscribeFirestore();
      } else {
        dispatch(setLoading(true));
        toast.error("User is not logged in.");
    }
    });
  }, []);

  return (
    <div className='font-marhey min-[1800px]:w-[1800px] mx-auto'>
      <BrowserRouter>
        {
          load ? <>
            <NavBar />
            <Routes>
              <Route path={'/home'} element={<TodoList />} />
              <Route path='/signup' element={<SignUp />} />
              <Route path='/login' element={<Login />} />
              <Route path='/' element={<TodoList />} />
              <Route path={`/home/task/:${taskId}`} element={<Todo/>} />
            </Routes>
            <Toaster position='top-right' />
          </> : <div className='flex justify-center mt-[20%] items-center'>
            <h1 className='text-clip bg-gradient-to-tr from-blue-800 to-blue-300 bg-clip-text text-transparent text-5xl max-sm:text-2xl font-bold'>Loading {dots} </h1>
            </div>
        }
      </BrowserRouter>
    </div>
  )
}