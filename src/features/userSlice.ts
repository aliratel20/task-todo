import { createSlice } from '@reduxjs/toolkit';

interface user {
  user: string | null
  isLoading: boolean
  username: string | null;
  task: {
    todoid: number
    task: string
    desc: string
    bdate: string
    edate: string
    done: boolean
    type: string
  }[]
  taskID:number|null
}

const initialState: user = {
  user: null,
  isLoading: true,
  task: [],
  taskID:null,
  username:null
};

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    loginUser: (state, action) => {
      state.user = action.payload;
      state.task = []
      state.isLoading = true
    },
    setUsername: (state,action) => {
      state.username = action.payload
    },
    logoutUser: (state) => {
      state.user = null;
      state.username = null;
      state.task = [];
      state.isLoading = true
    },
    setLoading: (state, action) => {
      state.isLoading = action.payload;
    },
    setTasks: (state, action) => {
      state.task = action.payload;
      state.isLoading = true
    },
    setTaskId: (state,action) => {
      state.taskID = action.payload
    }
  },
});

export const { loginUser, logoutUser, setLoading, setTasks,setTaskId,setUsername} = userSlice.actions;
