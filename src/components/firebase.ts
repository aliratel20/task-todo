// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app"
import { getFirestore } from "firebase/firestore"
import {
    getAuth,
    createUserWithEmailAndPassword,
    signInWithEmailAndPassword,
    signOut,
    onAuthStateChanged,
  } from "firebase/auth"
const firebaseConfig = {
    apiKey: "AIzaSyAsSxMvLw9Ehu7UQSANKiQuAfJnI0xBpP8",
    authDomain: "todo-app-9ff49.firebaseapp.com",
    projectId: "todo-app-9ff49",
    storageBucket: "todo-app-9ff49.appspot.com",
    messagingSenderId: "368668868286",
    appId: "1:368668868286:web:237c62e34db1f592c6a8ef"
};
// Initialize Firebase
const app = initializeApp(firebaseConfig)
const auth = getAuth(app)
const db = getFirestore(app)
export {db }
export {
    auth,
    createUserWithEmailAndPassword,
    signInWithEmailAndPassword,
    signOut,
    onAuthStateChanged,
}