import React from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../app/store'; // Adjust the path based on your project structure
import { Link, useParams } from 'react-router-dom';

export const Todo: React.FC = () => {
    const number = useParams();
    console.log('number ...'+number.null);
    const load = useSelector((state: RootState) => {
        return state.data.user.isLoading
    })
    const taskDetails = useSelector((state: RootState) => {
        // Fetch task details from the Redux state based on the taskId
        // Adjust this logic based on your actual Redux state structure
        return state.data.user.task.find((task) => task.todoid === Number(number.null));
    });
    // Implement the component logic to display task details

    return (
        <div className='flex justify-center flex-col items-center mt-20 max-w-[600px] mx-auto'>
            <div className='text-center p-4'>
            {load ? <div>
                {
                    taskDetails ? <>
                    <h2 className='text-4xl '>{taskDetails?.task}</h2>
                    <p className='text-md'>{taskDetails?.desc}</p>
                    <div>
                        <span className='text-sm text-green-400'>{taskDetails?.bdate}</span> /
                        <span className='text-sm text-red-400'>{taskDetails?.edate}</span>
                    </div>
                    <div><p className='text-xl text-red-800'>{taskDetails?.type}</p></div>
                    <div><p className='text-xl text-blue-700'>{taskDetails?.done ? 'finshed' : 'not finished'}</p></div>
                </> : <div className='bgstar text-purple-400 p-28 max-sm:p-16'><h1 className='text-6xl max-sm:text-2xl bg-[#f3f6fb] p-4 rounded-3xl shadow-2xl sm:p-2'>404 <br />not Found</h1></div> }
            </div> : <div><h1 className='text-6xl text-purple-600'>loading ..</h1></div> }
            </div>
            {taskDetails ? <Link className='bg-white my-4 font-bold border-4 border-white rounded-3xl shadow-2xl p-2' to={'/home'}> <span className='text-clip bg-gradient-to-tr from-blue-800 to-blue-300 bg-clip-text text-transparent '> Return to TodoList</span></Link> : ''}
        </div>
    );
};
