import React, { useEffect, useState, useCallback, ChangeEvent } from "react";
import { Input } from "./utilities/Input";
import { Textarea } from "./utilities/Input";
import toast from "react-hot-toast";
import { db, auth, onAuthStateChanged } from "./firebase.ts";
import search from '../img/icons/magnifying-glass.png'
import { collection, addDoc, onSnapshot, doc, deleteDoc, updateDoc } from "firebase/firestore";
import travel from "../img/icons/travel-itinerary.png";
import family from "../img/icons/family.png";
import pro from "../img/icons/programming.png";
import study from "../img/icons/study.png";
import click from "../img/icons/right-click.png"
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../app/store.ts";
import { setTasks } from "../features/userSlice.ts";
import { Link, useNavigate } from "react-router-dom";


interface Lists {
  id: string;
  todoid: number;
  task: string;
  done: boolean;
  type: string;
}


export const TodoList: React.FC = () => {
  const firestore = db;
  const Navigate = useNavigate()
    const userLog = useSelector((state: RootState) => state.data.user.user);
    const load = userLog ? true : false

  const [Todo, setTodo] = useState<Lists[]>([]);
  const [Task, setNewTask] = useState<string>("");
  const [Desc, setDesc] = useState<string>("");
  const [BDate, setBDate] = useState<string>("");
  const [EDate, setEDate] = useState<string>("");
  const [FormOption, setFormOption] = useState<string>("");
  const [ListOption, setListOption] = useState<string>("");
  const [filterTodo, setFilterTodo] = useState<Lists[]>([]);
  const [searchQuery, setSearchQuery] = useState<string>("");

  const dispatch = useDispatch();
  const userTasks = useSelector(
    (state: RootState) => state.data.user.task
  );
  console.log(userTasks)
  let userId : string | undefined
  auth.onAuthStateChanged((Auth) => {
    userId = Auth?.uid
  })

  useEffect(() => {
    let filteredItems = Todo;

    // Filter by ListOption
    if (ListOption !== "") {
      filteredItems = filteredItems.filter((item) => item.type === ListOption);
    }

    // Filter by searchQuery
    if (searchQuery !== "") {
      filteredItems = filteredItems.filter((item) =>
        item.task.toLowerCase().includes(searchQuery.toLowerCase())
      );
    }
    setFilterTodo(filteredItems)
  }, [Todo, ListOption, searchQuery]);

  useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, (user) => {
      if (user) {
        const userId = user.uid;
        const todoCollection = collection(firestore, 'users', userId, 'todos');

        const unsubscribeFirestore = onSnapshot(todoCollection, (querySnapshot) => {
          const todolist = querySnapshot.docs.map((doc) => {
            const data = doc.data();
            return {
              id: doc.id,
              todoid: data.todoid,
              task: data.task,
              done: data.done,
              type: data.type,
            };
          });
          const tododetails = querySnapshot.docs.map((doc) => {
            const data = doc.data();
            return {
              todoid: data.todoid,
              task: data.task,
              desc: data.desc,
              bdate: data.bdate,
              edate: data.edate,
              done: data.done,
              type: data.type
            };
          });
          dispatch(setTasks(tododetails));
          console.log("todolist:", todolist);
          
          setTodo(todolist);
        });
        return () => unsubscribeFirestore();
      } else {
        setTodo([]);
      }
    });

    return () => unsubscribe();
  }, []);

  const handelSelectForm = (e: ChangeEvent<HTMLSelectElement>) => {
    setFormOption(e.target.value);
  };
  const handelSelectList = (e: ChangeEvent<HTMLSelectElement>) => {
    setListOption(e.target.value);
  };


  const handleClick = async () => {
    if (Task && Desc && BDate && EDate && FormOption) {
      const currentUserId = auth.currentUser?.uid;
      if(currentUserId) {
        const todoCollection = collection(firestore, 'users', currentUserId, 'todos');
        const newTask = {
          todoid: Todo.length + 1,
          task: Task,
          desc: Desc,
          bdate: BDate,
          edate: EDate,
          done: false,
          type: FormOption,
        };
  
        await addDoc(todoCollection, newTask);
        dispatch(setTasks([...userTasks, newTask]));
  
        setNewTask("");
        setDesc("")
        setBDate("")
        setEDate("")
        setFormOption("")
        toast.dismiss();
      }
    } else {
      toast.dismiss();
      toast.error("Please fill out all form Inputs");
    }
  };
  console.log(userTasks)
  const handleDone = useCallback(async (id: string) => {
    if(userId) {
      try {
        const docRef = doc(firestore, 'users', userId, 'todos', id);
        await deleteDoc(docRef);
        setTodo((prevTodo) => prevTodo.filter((item) => item.id !== id));
      } catch (error) {
        console.error("Error deleting task:", error);
      }
    }else {
      return
    }
  }, [userId]);
  const handelfinish = async (taskId: string, currentDone: boolean) => {
    try {
      const taskToUpdate = Todo.find((task) => task.id === taskId);
  
      if (taskToUpdate && userId) {
        const updatedDone = !currentDone;

        setTodo((prevTodo) =>
          prevTodo.map((task) =>
            task.id === taskId ? { ...task, done: updatedDone } : task
          )
        );

        const docRef = doc(firestore, 'users', userId, 'todos', taskId);
        await updateDoc(docRef, { done: updatedDone });
  
        
        toast.success(updatedDone ? 'Task marked as finished' : 'Task marked as unfinished');
      }
    } catch (error) {
      console.error('Error updating task status:', error);
      // Handle error, show an error toast, etc.
    }
  };
  
  if(!userLog && !load) {
    Navigate('/login')
    return (
      null
    )
  }
  return (
    <div className="relative pt-20 grid items-center grid-cols-[35%_minmax(65%,_1fr)] md:px-20 max-md:grid-cols-1 max-md:mx-10 max-sm:mx-5 gap-10 text-xl max-sm:text-sm">
      <div className="flex flex-col gap-5  p-4">
        <Input
          text="Title of Task"
          type="text"
          onChange={setNewTask}
          className="bg-white font-marhey p-2 border-[3px] rounded-lg border-dashed"
          value={Task}
        />
        <Textarea
          text="Description of Task"
          rows={8}
          className="bg-white font-marhey border-[3px] rounded-lg border-dashed"
          onChange={setDesc}
          value={Desc}
        />
        <Input
          type="datetime-local"
          text="date and time of beginning"
          className="bg-white font-marhey p-2 border-[3px] border-dashed rounded-lg"
          onChange={setBDate}
          value={BDate}
        />
        <Input
          type="datetime-local"
          text="date and time of ending"
          className="bginput font-marhey p-2 border-[3px] border-dashed rounded-lg"
          onChange={setEDate}
          value={EDate}
        />
        <select
          onChange={handelSelectForm}
          className="text-center text-gray-700 font-marhey bginput p-2 border-[3px] border-dashed border-gray-400 rounded-lg"
        >
          <option value="" selected>
            select type of task
          </option>
          <option value="work">Work</option>
          <option value="family">Family</option>
          <option value="education">Education</option>
        </select>
        <button
          className="text-white font-bold bg-gradient-to-tr from-[#df674c] to-blue-200  p-2 rounded-xl"
          onClick={handleClick}
        >
          Add task
        </button>
      </div>
      <div className="p-4 rounded-3xl grid grid-cols-1 justify-items-center">
        <div className="grid justify-items-center text-center">
          <div className="relative flex justify-around z-10 rounded-full">
            <div className="grid justify-items-center">
              <img
                src={study}
                className="w-[50%] max-sm:w-[80%] mt-10 animate-spin-slow"
                alt=""
              />
            </div>
            <div className="grid justify-items-center">
              <img
                src={travel}
                className="w-[50%] max-sm:w-[80%] animate-spin-slow "
                alt=""
              />
            </div>
            <div className="grid justify-items-center">
              <img
                src={pro}
                className="w-[50%] max-sm:w-[80%] animate-spin-slow mt-12"
                alt=""
              />
            </div>
            <div className="grid justify-items-center">
              <img
                src={family}
                className="w-[50%] max-sm:w-[80%] animate-spin-slow mt-2"
                alt=""
              />
            </div>
          </div>
        </div>
        <div className="bgstar p-4  w-full rounded-3xl flex flex-col flex-wrap justify-center items-center gap-2 my-1">
          <div className="flex flex-wrap gap-2 justify-around items-center text-center max-sm:mt-4">
            <div className="relative items-center">
              <img src={search} className="absolute z-10 top-1/2 transform -translate-y-1/2 right-2 w-6" alt="" />
              <Input value={searchQuery} onChange={setSearchQuery} className="bg-white border-4 border-dashed border-red-300 p-2  rounded-lg " type="text" text={""} />
            </div>
            <div className="bg-white border-4 border-dashed border-red-300 max-sm:text-sm p-1">
              <h2>filter by :</h2>
              <select value={ListOption} onChange={handelSelectList} name="" id="">
                <option value="" selected>
                  All
                </option>
                <option value="work">Work</option>
                <option value="family">Family</option>
                <option value="education">Education</option>
              </select>
            </div>
          </div>
          <ul className="font-marhey bg-white border-4 border-dashed border-red-300 overflow-y-auto min-[1120px]:w-[70%] h-[20.3rem]">
            {filterTodo.sort((a, b) => a.todoid - b.todoid).map((e) => (
              <li className="whitespace-normal break-words p-4 max-sm:text-sm" key={e.id}>
                <Link className={`${e.done ? 'line-through' : ''}  flex items-start gap-1`} to={`/home/task/${e.todoid}`}>
                <img src={click} alt="click" className="w-[7%] max-sm:w-[10%]"/>
                 {e.todoid} . {e.task}.
                 </Link>
                <button 
                onClick={()=>handelfinish(e.id,e.done)} 
                className="font-marhey text-green-300"> 
                {e.done ? 'Finished' : 'Finish' } </button> /
                <button
                  onClick={() => handleDone(e.id)}
                  className="text-red-300"
                >
                  Delete!
                </button>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
};

{/* <button onClick={() => HandelTask(e.todoid)}> {e.todoid} - {e.task}. </button> */ }
