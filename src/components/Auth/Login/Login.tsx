import React, { FormEvent, useState } from "react";
import { Input } from "../../utilities/Input";
import { auth } from '../../firebase.ts';
import { signInWithEmailAndPassword } from 'firebase/auth'
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { loginUser, setLoading } from "../../../features/userSlice.ts"

import toast from "react-hot-toast";



export const Login: React.FC = () => {

    const [email, setEmail] = useState<string>('');
    const [password, setPassword] = useState<string>('');
    const navigate = useNavigate();
    const dispatch = useDispatch();

    const handleLogin = async (email: string, password: string, e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        try {
            await dispatch(setLoading(false))
            navigate('/home')
            await signInWithEmailAndPassword(auth, email, password)
            const user = auth.currentUser;
            await dispatch(
                loginUser({
                    uid: user?.uid,
                    username: user?.displayName,
                    email: user?.email,
                })
            );
            dispatch(setLoading(true))
            toast.success('logIn Successful')
            // ...
        } catch (error:unknown) {
            if(error instanceof Error) {
                await dispatch(setLoading(true))
                const errorMessage = error.message;
                await toast.error(errorMessage)
                await toast.error('login failed')
            }else {
                toast.error('An unknown error occurred.')
            }
            // ..
        }
    }
    return (
        <div className="bgstar p-10 max-sm:p-5 rounded-3xl mt-20 w-1/2 max-md:w-3/4 max-sm:w-5/6 mx-auto">
            <div className="w-1/2 mx-auto p-10 relative flex justify-around z-10 rounded-full">
                <h2 className="text-3xl text-[#9892be] font-extrabold shadow-2xl bg-[#f3f6fb] p-6 rounded-b-md rounded-t-lg">LOGIN</h2>
            </div>
            <div className="my-6">
                <form className="flex flex-col gap-6" onSubmit={(e) => handleLogin(email, password, e)} action="">
                    <Input className="bg-white" onChange={setEmail} text="Email" type="email" value={email} />
                    <Input className="bg-white" onChange={setPassword} text="Password" type="password" value={password} />
                    <button className="text-white font-bold bg-gradient-to-tr from-[#df674c] to-blue-200  p-2 rounded-xl">Login</button>
                </form>
            </div>
        </div>
    )
}