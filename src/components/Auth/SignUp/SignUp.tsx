import React, {FormEvent, useState } from "react";
import { Input } from "../../utilities/Input";
import { auth } from '../../firebase.ts';
import { createUserWithEmailAndPassword, updateProfile } from 'firebase/auth'
import { useNavigate } from "react-router-dom";

import toast from "react-hot-toast";
import { setLoading } from "../../../features/userSlice.ts";
import { useDispatch } from "react-redux";



export const SignUp: React.FC = () => {

    const [email, setEmail] = useState<string>('');
    const [password, setPassword] = useState<string>('');
    const [username, setUsername] = useState<string>('');
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const handleSignUp = async (email: string, password: string, username: string, e:FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        if(username.length <=10){
            try {
                await dispatch(setLoading(false))
                await createUserWithEmailAndPassword(auth, email, password);
                await updateProfile(auth.currentUser!, {
                    displayName: username
                })
                await navigate('/login')
                await dispatch(setLoading(true))
                await toast.success('SignUp Successful')
                // ...
            } catch (error:unknown) {
                await dispatch(setLoading(true))
                if(error instanceof Error) {
                    const errorMessage = error.message;
                    await toast.error(errorMessage)
                    await toast.error('SignUp failed')
                }else {
                    await toast('An unknown error occurred.')
                }
                // ..
            }
        }else {
            toast("username should be less than 11 letters")
        }
    }
    return (
        <div className="bgstar p-10 max-sm:p-5 rounded-3xl mt-20 w-1/2 max-md:w-3/4 max-sm:w-5/6 mx-auto">
            <div className="w-1/2 mx-auto p-10 relative flex justify-around z-10 rounded-full">
                <h2 className="text-3xl text-[#9892be] font-extrabold shadow-2xl bg-[#f3f6fb] p-6 rounded-b-md rounded-t-lg">SignUp</h2>
            </div>
            <div className="my-6">
                <form className="flex flex-col gap-6" onSubmit={(e) => handleSignUp(email, password, username, e)} action="">
                    <Input className="bg-white" onChange={setUsername} text="Username" type="text" value={username} />
                    <Input className="bg-white" onChange={setEmail} text="Email" type="email" value={email} />
                    <Input className="bg-white" onChange={setPassword} text="Password" type="password" value={password} />
                    <button className="text-white font-bold bg-gradient-to-tr from-[#df674c] to-blue-200  p-2 rounded-xl">Create Account</button>
                </form>
            </div>
        </div>
    )
}