import React from 'react'
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom'
import { RootState } from '../app/store';
import { signOut } from 'firebase/auth';
import { logoutUser, setLoading } from '../features/userSlice';
import toast from 'react-hot-toast';
import { auth } from './firebase';
import { useNavigate } from 'react-router-dom';


export const NavBar: React.FC = () => {
    const dispatch = useDispatch()
    const Navigate = useNavigate()
    const USER = useSelector((state: RootState) => state.data.user.username);
    const handleLogout = async (e:React.MouseEvent<HTMLButtonElement>) => {
        e.preventDefault()
        try {
            dispatch(setLoading(false))
            await signOut(auth);
            dispatch(logoutUser()); // Assuming you have a logout action to reset the user state
            Navigate('/login'); // Redirect to the login page or any other page after logout
            dispatch(setLoading(true))
            toast.success('Logout successful');
        } catch (error:unknown) {
            if(error instanceof Error) {
                console.error('Error during logout:', error);
                toast.error('Logout failed '+error);
            }
        }
    }

    return (
        <nav className='fixed top-0 w-[100%] bg-white flex z-20 justify-around items-center p-2 shadow-md min-[1800px]:w-[1800px] mx-auto'>
            <h1 className="text-clip bg-gradient-to-tr from-blue-800 to-blue-300 bg-clip-text text-transparent text-3xl max-sm:text-lg font-bold font-marhey">
                {(USER) && (<span className=' text-lg max-sm:text-xs font-bold rounded-xl'>{USER}'s</span>)}<Link to={(USER) ? '/home' : '/login'}>Todo List</Link>
            </h1>
            <div className='flex gap-2'>
                {
                    (USER) ? <button onClick={e => handleLogout(e)} className='text-lg max-sm:text-sm font-bold text-red-400 border-4 border-dashed py-1 px-4 rounded-xl'>logOut</button>
                        : <div className='flex gap-2'>
                            <button className='text-lg max-sm:text-sm font-bold text-red-400 border-4 border-dashed py-1 px-4 rounded-xl'><Link to={'/signup'}>SignUp</Link></button>
                            <button className='text-lg max-sm:text-sm font-bold text-red-400 border-4 border-dashed py-1 px-4 rounded-xl'><Link to={'/login'}>LogIn</Link></button>
                        </div>
                }
            </div>
        </nav >
    )
}