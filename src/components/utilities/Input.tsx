import React, { ChangeEvent } from 'react'

interface inputText {
    value: string
    className?: string
    type?: string
    text: string
    rows?: number
    onChange?: (value: string) => void | undefined;
}


export const Input: React.FC<inputText> = ({ className, value, onChange, type, text }) => {
    const handleInputChange = (e: ChangeEvent<HTMLInputElement>) => {
        if(onChange) {
            onChange(e.target.value);
        }
    };
    return (
        <div className="relative">
            <input
                id="floating_outlined"
                value={value}
                className={`${className ? className : ''} border-[3px] border-dashed block px-2.5 pb-2.5 pt-4 w-full text-lg text-gray-900 bg-transparent rounded-lg border-1 border-gray-400 appearance-none focus:outline-none focus:ring-0 focus:border-blue-600 peer `}
                type={type}
                onChange={handleInputChange}
            />
            <label htmlFor="floating_outlined" className="absolute text-md text-gray-500 duration-300 transform -translate-y-4 scale-75 top-2 z-10 origin-[0] rounded-full bg-white px-2 peer-focus:px-2 peer-focus:text-blue-600 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-4 rtl:peer-focus:translate-x-1/4 rtl:peer-focus:left-auto start-1">{text}</label>
        </div>
    )
}

export const Textarea: React.FC<inputText> = ({ className, value, onChange, rows, text }) => {
    const handleInputChange : React.ChangeEventHandler<HTMLTextAreaElement>  = (e) => {
        if(onChange) {
            onChange(e.target.value);
        }
    };
    return (
        <div className="relative">
            <textarea
                id="floating_outlined"
                rows={rows}
                value={value}
                className={`${className ? className : ''} block px-2.5 pb-2.5 pt-4 w-full text-lg text-gray-900 bg-transparent rounded-lg border-1 border-gray-400 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer `}
                onChange={handleInputChange}
            />
            <label htmlFor="floating_outlined" className="absolute text-md text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-4 scale-75 top-2 z-10 origin-[0] bg-white dark:bg-gray-900 px-2 peer-focus:px-2 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-4 rtl:peer-focus:translate-x-1/4 rtl:peer-focus:left-auto start-1">{text}</label>
        </div>
    )
}