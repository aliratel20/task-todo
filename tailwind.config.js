/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {},
    animation: {
      'spin-slow': 'spin 3s linear infinite',
    },
    keyframes: {
      spin: {
        '0%, 100%': { transform: 'rotate(-8deg)' },
        '50%': { transform: 'rotate(8deg)' },
      }
    },
    fontFamily: {
      marhey: "'Marhey', sans-serif"
    }
  },
  plugins: [],
}

